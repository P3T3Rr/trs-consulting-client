import { createCategorieAdapter } from "../adapters/categorie-adapter";
import { setServicesFilter } from "../store/app/servicesFilter-slice";
import { collection, onSnapshot, query } from "firebase/firestore";
import { setServices } from "../store/app/services-slice";
import { Spinner } from "../components/spinner/spinner";
import { Card } from "../components/services-card/card";
import { db } from "../services/firebase-config";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

export const Home = () => {
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        update();
    }, []);

    const update = async () => {
        const snap = query(collection(db, "categories"));
        onSnapshot(snap, (querySnapshot) => {
            let info = [];
            querySnapshot.forEach((doc) => {
                if (doc.data() !== null) {
                    info.push(createCategorieAdapter(doc.id, doc.data()));
                }
            })
            setLoading(false);
            dispatch(setServices(info));
            dispatch(setServicesFilter(info));
        });
    }
    return (
        <div className="page">
            {loading ?
                <div className="spinnerContainer"><Spinner /></div>
                :
                <Card />
            }
        </div>
    );
}
