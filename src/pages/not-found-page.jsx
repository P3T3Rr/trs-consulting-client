import { useSelector } from "react-redux";

export const NotFoundPage = () => {

    const language = useSelector(state => state.language);

    return (
        <div className="page">
            {language === "spanish" ?
                <h1>¡Página no encontrada!</h1>
                :
                <div>
                <h1>Page not found!</h1>
                    </div>
            }
        </div>
    )
}