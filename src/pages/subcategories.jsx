import { CardSubcategorie } from "../components/subcategorie-card/card-subcategorie";
import { collection, onSnapshot, query } from "firebase/firestore";
import { db } from "../services/firebase-config";
import { createSubcategorieAdapter } from "../adapters/subcategorie-adapter";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Alert } from "../components/alert/Alert";
import "../components/subcategorie-card/styles.css"



export const Subcategories = () => {

  let { id } = useParams();
  const [subcategories, setSubcategories] = useState([]);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    getSubcategories();
  }, []);

  const getSubcategories = async () => {
    const snap = query(collection(db, "subcategories"));
    onSnapshot(snap, (querySnapshot) => {
      const data = [];
      querySnapshot.forEach((doc) => {
        if (doc.data() !== null) {
          if (doc.data().category === id) {
            data.push(createSubcategorieAdapter(doc.id, doc.data()));
          }
        }
      });
      setSubcategories(data);
    });
  };

  return (

    <>
      <div className="principalContainerSubCategorie">
        {subcategories.map((subcategorie, index) => (
          <CardSubcategorie key={index} Text={subcategorie.column_1} Description={subcategorie.column_3} setOpen={setOpen} />
        ))}
      </div>
      {open && <div className="alertSubcategorie">
        <Alert text="Ya lo tienes agregado en el carrito" setOpen={setOpen}> </Alert>
      </div>}


    </>

  );
};
