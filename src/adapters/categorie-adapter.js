export const createCategorieAdapter = (id, categorie) => {
    return {"id":id, "name": String(categorie.name), "description": String(categorie.description), "images": categorie.images};
}