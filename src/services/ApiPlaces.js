
const NOMINATIM_BASE_URL = "https://nominatim.openstreetmap.org/search?";

const requestOptions = {
    method: "GET",
    redirect: "follow",
};

export const ApiPlaces = async(queryString,setPlaces,setloadingPlaces)=>{

    setloadingPlaces(true)
    const response = await fetch(`${NOMINATIM_BASE_URL}${queryString}`, requestOptions)
    const responseJSON = await  response.json()
    setPlaces(responseJSON);
    setloadingPlaces(false)

}