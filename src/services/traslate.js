export const traslate = (info, setInfo, setLoading, language) => {
    if (language === "spanish") {
        setInfo(info);
    }
    else {
        setLoading(true);
        console.log("Llamada al api de traduccion");
        let urlApi = `https://api.mymemory.translated.net/get?q=${info}&langpair=es|en`;
        fetch(urlApi).then(res => res.json()).then(data => {
            console.log(data.responseData.translatedText);
            setInfo(data.responseData.translatedText);
            setLoading(false);
        })
    }
}