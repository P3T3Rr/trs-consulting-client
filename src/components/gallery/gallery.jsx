import { query, collection, orderBy, onSnapshot } from "firebase/firestore";
import { createGaleryAdapter } from "../../adapters/galery-adapter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { db } from "../../services/firebase-config";
import { Spinner } from "../spinner/spinner";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import './styles.css';

export const Galery = () => {
    const language = useSelector(state => state.language);
    const [images, setImages] = useState([]);
    const [selectedImage, setSelectedImage] = useState('');
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        update();
    }, [])

    const update = async () => {
        const snap = query(collection(db, "galery"), orderBy("date", "desc"));
        onSnapshot(snap, (querySnapshot) => {
            const galery = [];
            querySnapshot.forEach((doc) => {
                if (doc.data() !== null) {
                    galery.push(createGaleryAdapter(doc.id, doc.data()));
                }
            })
            setLoading(false);
            setImages(galery);
        });
    }

    return (
        <div className="galeryContainer">
            {loading ?
                <div className="spinnerContainer"><Spinner /></div>
                :
                <div className='cardContainer'>
                    {images.length > 0 ?
                        images.map((image, index) =>
                            <div className="card" key={index}>
                                <img className="imageCard" src={image.url} onClick={() => setSelectedImage(image.url)} />
                            </div>
                        )
                        :
                        <div className='emptyCollection'>
                            {language === "spanish" ?
                                <h1>No hay imágenes</h1>
                                :
                                <h1>No images to show</h1>
                            }
                        </div>
                    }
                </div>
            }

            {selectedImage !== "" ?
                <div className="modalGalery" >
                    <button onClick={() => setSelectedImage("")} className="modalCloseGalery">
                        <FontAwesomeIcon icon={faTimes} />
                    </button>
                    <div className="modalContainerGalery">
                        <a href={selectedImage} target="_blank">
                            <img src={selectedImage} />
                        </a>
                    </div>
                </div>
                :
                null
            }
        </div>
    )
}