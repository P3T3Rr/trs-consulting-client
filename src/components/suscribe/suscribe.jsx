import React, { useState } from 'react'
import { useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from "react-redux";
import { openCloseSuscribe } from "../../store/app/suscribeslice";
import { createData } from "../../services/create"
import './styles.css'

const Suscribe = () => {
  const open = useSelector(state => state.suscribe);
  const language = useSelector(state => state.language);
  const [name, setName] = useState('')
  const [mail, setMail] = useState('')
  const [error, setError] = useState(false)

  const dispatch = useDispatch();

  const openSuscribe = () => {
    dispatch(openCloseSuscribe());
    setName('');
    setMail('');
  }

  const handleChangeName = (event) => {
    setName(event.target.value)
  }

  const handleChangeMail = (event) => {
    setMail(event.target.value)
  }

  const close = () => {
    openSuscribe()
    setName('');
    setMail('');
  }
  console.log(error)
  const save = () => {
    if (name === "" || mail === "") {
      setError(true)
    }
    else {
      const data = { 'name': name, 'mail': mail };
      createData("suscribers", data);
      openSuscribe()
      setName('');
      setMail('');
    }
  }

  return (
    <>
      {open ?
        <div className='modal'>
          {language === "spanish" ?
            <div className="modal__container">
              <h1 style={{ color: "#3d3d3d" }}>Suscribirme</h1>
              <button onClick={() => close()} title="Cerrar" className="modal__close__x">
                <FontAwesomeIcon icon={faXmark} />
              </button>
              <div className="input-group">
                <input onChange={handleChangeName} name="name" required id="name" type="text" className="inputSee" value={name} />
                <label htmlFor="name" className="input-label">Nombre</label>
              </div>
              <div className="input-group">
                <input onChange={handleChangeMail} name="mail" required id="mail" type="text" className="inputSee" value={mail} />
                <label htmlFor="mail" className="input-label">Correo</label>
              </div>
              <div className='containerButtonsSubscribe'>
                <button onClick={() => openSuscribe()} className="buttonCancelStyle">Cancelar</button>
                <button onClick={save} className="buttonStyle" >Suscribirme</button>
              </div>


              <div className='AlertsModal'>
                {error ?
                  <div className='notificationModal' >
                    <button title='Cerrar' className='close-button' onClick={() => setError(false)}>
                      <FontAwesomeIcon icon={faXmark}
                      /></button>
                    <p>Campos vacíos</p>
                  </div>
                  :
                  null
                }
              </div>
            </div>
            :
            <div className="modal__container">
              <h1 style={{ color: "#3d3d3d" }}>Subscribe</h1>
              <button onClick={() => close()} title="Close" className="modal__close__x">
                <FontAwesomeIcon icon={faXmark} />
              </button>
              <div className="input-group">
                <input onChange={handleChangeName} name="name" required id="name" type="text" className="inputSee" value={name} title="Name" />
                <label htmlFor="name" className="input-label">Name</label>
              </div>
              <div className="input-group">
                <input onChange={handleChangeMail} name="mail" required id="mail" type="text" className="inputSee" value={mail} title="Mail" />
                <label htmlFor="mail" className="input-label">E-mail</label>
              </div>
              <button onClick={() => openSuscribe()} className="buttonCancelStyle" title='Close'>Cancel</button>
              <button onClick={save} className="buttonStyle" title='Save'>To Subscribe</button>


              <div className='AlertsModal'>
                {error ?
                  <div className='notificationModal' >
                    <button title='Close' className='close-button' onClick={() => setError(false)}>
                      <FontAwesomeIcon icon={faXmark}
                      /></button>
                    <p>Empty fields</p>
                  </div>
                  :
                  null
                }
              </div>
            </div>
          }
        </div>

        : null
      }
    </>
  )
}

export default Suscribe