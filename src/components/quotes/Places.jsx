import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faMapLocation, faLocationDot, faSearchLocation } from "@fortawesome/free-solid-svg-icons"
import { useState } from "react";
import { useDispatch } from "react-redux";
import { setStateCenter } from '../../store/app/mapSlice'
import { setStateMarker } from '../../store/app/mapMarkerSlice'
import { Input } from "./Input";
import { useForm } from "../../hooks/useForm";
import { Spinner } from "../spinner/spinner";
import { ApiPlaces } from '../../services/ApiPlaces'

export const Places = ({dataLanguage}) => {

    const dispatch = useDispatch();

    const [places, setPlaces] = useState([])

    const [loadingPlaces, setloadingPlaces] = useState(null)

    const { onInputChange, placeName } = useForm(
        { placeName: '' }
    )

    const handlePlace = () => {
        const params = {
            q: placeName,
            format: "json",
            addressdetails: 1,
            limit: 4,
            polygon_geojson: 0,
        };
        const queryString = new URLSearchParams(params).toString();
        ApiPlaces(queryString, setPlaces, setloadingPlaces)
    }

    const handleNewCenter = (lat, lon) => {
        const newPlace = { "center": { lat: parseFloat(lat), lng: parseFloat(lon) } }
        const newPlaceMarker = { "marker": { lat: parseFloat(lat), lng: parseFloat(lon) } }
        dispatch(setStateCenter(newPlace))
        dispatch(setStateMarker(newPlaceMarker))

    }

    return (
        <>
            <div className="formMapResposive">

                <Input
                    onInputChange={onInputChange}
                    value={placeName}
                    name="placeName"
                    type={"text"}
                    icon={faMapLocation}
                    placeholder={dataLanguage.searchPlaceHolder}>
                </Input>

                <button title={dataLanguage.titleSearchButton}
                    className='SearchButton'
                    onClick={() => { handlePlace() }}
                >  <FontAwesomeIcon className='IconColor' icon={faSearchLocation} />
                    {dataLanguage.searchButton} </button>


                <div className="containerPlace" >

                    {loadingPlaces ? (
                        <div className="spinerPlaces">
                            <Spinner />
                        </div>

                    ) :
                        (

                            loadingPlaces !==null && places.length === 0 ? (
                                <span style={{ color: "black", fontSize:"0.8rem" }}>{dataLanguage.AnyResultsPlace}</span>) 
                                : 
                                (
                                places.map((place) => (
                                    <div key={place.place_id}>
                                        <div title={dataLanguage.titlePlace} className="bodyPlaces" onClick={() => { handleNewCenter(place.lat, place.lon) }}  >
                                            <FontAwesomeIcon className='IconColor' icon={faLocationDot} />
                                            <span className="namePlaces"> {place.display_name} </span>
                                        </div>
                                        <hr key={place.osm_id} className="linePlaces" />
                                    </div>

                                ))

                            )


                        )}

                </div>
            </div>
        </>
    )
}
