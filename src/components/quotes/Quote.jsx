import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import ReactWhatsapp from "react-whatsapp";
import { Places } from "./Places";
import { openCloseQuote } from "../../store/app/quotesSlice";
import { Input } from "./Input";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons";
import { useForm } from "../../hooks/useForm";
import { Alert } from "../alert/Alert";
import whatsAppIcon from "../../assets/WhatsApp.svg.webp";
import "./StylesQuotes.css";

export const Quote = ({ language }) => {
  const { data } = language;
  const dispatch = useDispatch();
  const marker = useSelector((state) => state.mapMarker);
  const [whatsappMessage, setWhatsappMessage] = useState("");
  const [allData, setAllData] = useState(false);
  const [message, setMessage] = useState("");
  const [errorMessage, setError] = useState(false);
  const subcategories = useSelector((state) => state.bodyCart);
  const originalLat = marker.marker.lat;
  const originalLng = marker.marker.lng;
  const { onInputChange, nameClient, phoneNumberClient, date, comment } =
    useForm({ nameClient: "", phoneNumberClient: "", date: "", comment: "" });

  useEffect(() => {
    if (
      nameClient !== "" &&
      phoneNumberClient !== "" &&
      date !== "" &&
      subcategories.length !== 0
      // && originalLat !== 10.4539194
      // && originalLng !== -84.2747255
    ) {
      const direction = `https://www.google.com/maps/dir/${originalLat},${originalLng}`;

      let subCategoriesWhatsApp = "";

      for (let index = 0; index < subcategories.length; index++) {
        subCategoriesWhatsApp += index + 1 + "-" + subcategories[index] + "\n";
      }

      setWhatsappMessage(
        "*_Cotización_*" +
          "\n\n*_Datos personales_*" +
          "\nNombre del cliente: " +
          nameClient +
          "\nNumero de teléfono: " +
          phoneNumberClient +
          "\nRequiere visita: " +
          date +
          "\nDireccion :" +
          direction +
          "\n \n*_SubCategorias a consultar :_* \n" +
          subCategoriesWhatsApp +
          " \n*_Comentarios:_* \n" +
          comment
      );
      setAllData(true);
    } else {
      setAllData(false);
    }
  }, [
    nameClient,
    phoneNumberClient,
    date,
    comment,
    originalLng,
    originalLat,
    subcategories,
  ]);

  const handleClick = () => {
    dispatch(openCloseQuote());
  };

  const quoteForm = () => {
    if (nameClient === "" || phoneNumberClient === "") {
      setMessage(data.ErrormessageEmptyInputs);
      setError(true);
    } else if (date === "") {
      setError(true);
      setMessage(data.ErrormessageEmptyOncoming);
    } else if (originalLat === 10.2539194 && originalLng === -84.2747255) {
      setError(true);
      setMessage(data.ErrormessageEmptyPlace);
    } else if (subcategories.length === 0) {
      setError(true);
      setMessage(data.ErrormessageSubcategorie);
    }
  };

  return (
    <div className="modalQuote">
      <div className="modalContainerQuote ">
        <button className="modalCloseQuote" onClick={() => handleClick()}>
          <FontAwesomeIcon icon={faTimes} />
        </button>

        <div className="modalTitle">
          <h1> {data.quoteTitle}</h1>
        </div>

        <div className="formQuote">
          <Input
            onInputChange={onInputChange}
            value={nameClient}
            name="nameClient"
            type={"text"}
            icon={faUser}
            placeholder={data.namePlaceholder}
          >
            {" "}
          </Input>

          <Input
            onInputChange={onInputChange}
            value={phoneNumberClient}
            name="phoneNumberClient"
            type={"number"}
            icon={faPhone}
            placeholder={data.phonePlaceholder}
          >
            {" "}
          </Input>

          <div className="selectionContainer">
            <div className="containerIconSelect">
              <FontAwesomeIcon className="IconColor" icon={faCalendarAlt} />
            </div>

            <div className="optionsContainer">
              <span>{data.inputOncoming} </span>
              <select
                onChange={onInputChange}
                value={date}
                name="date"
                className="drop_down"
              >
                <option value={""}>{data.opcionSelect}</option>
                <option>{data.opcionSi}</option>
                <option>{data.opcionNo}</option>
              </select>
            </div>
          </div>
        </div>

        <div className="mapTitle">
          <h3>{data.titleMap}</h3>
        </div>

        <div className="formMap ">
          {/* <Map> </Map>   */}
          <div className="simulacionMapa "></div>

          <Places dataLanguage={data}> </Places>
        </div>

        <div className="SubCategorieTitle">
          {/* METER AL TRADUCTOR */}
          <h3>{data.titleSubcategorie} </h3>
        </div>
        <div className="subCategoriesContainer">
          {subcategories.length === 0 ? (
            <span style={{ color: "black", fontSize: "0.8rem" }}>
              {data.SubcategorieEmpty}
            </span>
          ) : (
            subcategories.map((subcategorie, index) => (
              <div className="subCategorieItem" key={index}>
                <span> {subcategorie} </span>
              </div>
            ))
          )}
        </div>
        <div className="textAreaContainer">
          <div className="CommentTitle">
            {/* METER AL TRADUCTOR */}
            <h3> {data.titleComment} </h3>
          </div>
          <textarea
            onChange={onInputChange}
            value={comment}
            name="comment"
            className="textArea"
            cols="90"
            rows="1"
          ></textarea>
        </div>

        {allData ? (
          <ReactWhatsapp
            className="buttonWhatsappDisable"
            number="60994896"
            onClick={quoteForm}
            message={whatsappMessage}
          >
            <img className="foto" src={whatsAppIcon} alt="WhatsApp Icon" />
            {data.whatsAppButton}
          </ReactWhatsapp>
        ) : (
          <button className="buttonWhatsappDisable" onClick={quoteForm}>
            <img className="foto" src={whatsAppIcon} alt="sin icono" />
            {data.whatsAppButton}
          </button>
        )}

        {errorMessage && (
          <div className="alertdiv">
            <Alert text={message} setOpen={setError}>
              {" "}
            </Alert>
          </div>
        )}
      </div>
    </div>
  );
};
