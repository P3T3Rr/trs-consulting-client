import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

export const Input = ({ type, icon, placeholder, onInputChange, value, name ,errors}) => {
    return (
        <>
            <div className='divForm'>
                <div>
                    <FontAwesomeIcon className='IconColor' icon={icon} />
                </div>
                <input value={value} onChange={onInputChange} name={name} className='form-control' type={type} placeholder={placeholder} />
            </div>
            {errors && <div className='errorForm'>{errors}</div>} 
        </>

    )
}
