import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react'
import './AlertStyle.css'

export const Alert = ({ text, setOpen }) => {
    useEffect(() => {
        setTimeout(close, 3500);
    }, [])

    function close (){
        setOpen(false)
    }

    return (
        <div className='notificationModal' >
            <button title='Cerrar' className='close-button' onClick={close}> 
            <FontAwesomeIcon className='iconAlert' icon={faXmark} 
            /></button>
            <p>{text}</p>
        </div>
    );
}
