import { useSelector, useDispatch } from 'react-redux'
import { openCloseCart } from '../../store/app/cartSlice'
import { Quote } from '../quotes/Quote';
import { MenuCart } from './MenuCart';
import languageSpanish from '../../apis/quote-spanish.json'
import languageEnglish from '../../apis/quote-english.json'


import './StylesCart.css'

export const ShoppingCart = () => {

    const language = useSelector(state => state.language);
    const openQuote = useSelector(state => state.quote);
    const openState = useSelector(state => state.cart);
    const dispatch = useDispatch();

    const handleClick = () => {
        dispatch(openCloseCart());
    }


    return (
        <>
            <div className={`bodyCart ${openState ? ('hide') : ''} `}>
                <MenuCart handleClick={handleClick} > </MenuCart>
            </div>
            {openQuote && ((language === "spanish" && <Quote language={languageSpanish} />) ||
                (language === "english" && <Quote language={languageEnglish} />))}
           

        </>
    )
}
