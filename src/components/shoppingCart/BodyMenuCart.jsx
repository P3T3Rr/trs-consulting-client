import { openCloseCart } from '../../store/app/cartSlice';
import { openCloseQuote } from '../../store/app/quotesSlice';
import { deleteSubcategoryToCart } from "../../store/app/bodyCartSlice";
import { subtractTotalCart } from "../../store/app/cartTotalSlice";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashCan, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';

export const BodyMenuCart = () => {
  const subcategories = useSelector((state) => state.bodyCart);

  const dispatch = useDispatch();

  const handleCloseCart = () => {
    dispatch(openCloseCart());
  }

  const handleOpenQuote = () => {
    dispatch(openCloseQuote());
  }
  const handleOptionsCart = (subcategorie) => {
    dispatch(deleteSubcategoryToCart(subcategorie));
    dispatch(subtractTotalCart());
  }

  return (
    <>
      <div className="menuCart">
        <h3 className='cartTitle'>Cotizaciones</h3>
        <div className='containerProductCart'>
          {subcategories.map((subcategorie, index) => (
            <div className='generalProduct' key={index}>
              <span className="productCart">
                {subcategorie}
              </span>
              <FontAwesomeIcon title='borrar elemento' onClick={() => handleOptionsCart(subcategorie)}
                className='faDeleteProductCart'
                icon={faTrashCan} />
            </div>
          ))}
        </div>
        <button className='btnDetails' onClick={handleOpenQuote}>Generar cotización

        </button>
      </div>
      <div onClick={handleCloseCart} className="shadow"></div>
    </>
  );
}
