import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { openCloseCart } from '../../store/app/cartSlice';
import { BodyMenuCart } from "./BodyMenuCart";
import { useDispatch } from "react-redux";

export const MenuCart = () => {

    const dispatch = useDispatch();

    return (
        <>
            <FontAwesomeIcon
                title='cerrar'
                onClick={() => dispatch(openCloseCart())}
                className='fa-closeCart'
                icon={faArrowRight} />
            <BodyMenuCart> </BodyMenuCart>
        </>
    )
}
