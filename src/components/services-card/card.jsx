import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import './styles.css';

export const Card = () => {

    const information = useSelector(state => state.filterServices);

    return (
        <div className='cardContainerServices'>
            {information.map((services, key) =>
                <Link key={key} className='serviceCard' to={services.name}>
                    <p className='serviceTittle'>{services.name}</p>                    
                    <img className='serviceBackground' src={services.images[1]} />
                    <img className='serviceIcon' src={services.images[0]} />
                </Link>
            )}
        </div>
    )
}