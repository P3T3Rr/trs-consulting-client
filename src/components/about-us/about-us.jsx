import { createGeneralInformationAdapter } from "../../adapters/general-information-adapter";
import { query, collection, onSnapshot } from "firebase/firestore";
import Logo from "../../assets/Logo (Fondo Claro).png";
import { db } from "../../services/firebase-config";
import { traslate } from "../../services/traslate";
import { Spinner } from "../spinner/spinner";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import './styles.css';

export const AboutUs = () => {
    const language = useSelector(state => state.language);
    const [information, setInformation] = useState("");
    const [informationTraslate, setInformationTraslate] = useState("");
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        update();
    }, []);

    useEffect(() => {
        traslate(information.aboutus, setInformationTraslate, setLoading, language);
    }, [language]);

    const update = async () => {
        const snap = query(collection(db, "general-information"));
        onSnapshot(snap, (querySnapshot) => {
            console.log("Llamada a la base de datos");
            let info;
            querySnapshot.forEach((doc) => {
                if (doc.data() !== null) {
                    info = createGeneralInformationAdapter(doc.id, doc.data());
                }
            })
            setLoading(false);
            setInformation(info);
            traslate(info.aboutus, setInformationTraslate, setLoading, language);
        });
    }

    return (
        <div className="page">
            {loading ?
                <div className="spinnerContainer"><Spinner /></div>
                :
                <div className='generalInformationContainer'>
                    {language === "spanish" ?
                        <h1>¿Quiénes somos?</h1>
                        :
                        <h1>About us?</h1>
                    }
                    <p>{informationTraslate}</p>
                    <div>
                        <img src={Logo} />
                    </div>
                </div>
            }
        </div>
    )
}