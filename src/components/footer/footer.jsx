import { createGeneralInformationAdapter } from "../../adapters/general-information-adapter";
import { faPhone, faEnvelope, faLaptopCode } from "@fortawesome/free-solid-svg-icons";
import { query, collection, onSnapshot } from "firebase/firestore";
import Instagram from '../../assets/socialNetworks/instagram.svg';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Facebook from '../../assets/socialNetworks/facebook.svg';
import { Developers } from "./developers/developers";
import { db } from "../../services/firebase-config";
import { useEffect, useRef, useState } from 'react';
import { Carrusel } from "./carrusel/carrusel";
import { useSelector } from 'react-redux';
import './styles.css';

export const Footer = () => {
    const language = useSelector(state => state.language);
    const [information, setInformation] = useState();
    const [loading, setLoading] = useState(true);
    const devRef = useRef();

    useEffect(() => {
        update();
    }, []);

    const openDevelopers = () => {
        devRef.current.classList.toggle('developersContainerToggle');
    }

    const update = async () => {
        const snap = query(collection(db, "general-information"));
        onSnapshot(snap, (querySnapshot) => {
            let info;
            querySnapshot.forEach((doc) => {
                if (doc.data() !== null) {
                    info = createGeneralInformationAdapter(doc.id, doc.data());
                }
            })
            setLoading(false);
            setInformation(info);
        });
    }

    return (
        <div className='footerContainer'>
            {!loading ?
                <footer>
                    <div className='labels'>
                        <p className='email-label'><FontAwesomeIcon icon={faEnvelope} /> {information.email}</p>
                        <p className='phone-label'><FontAwesomeIcon icon={faPhone} /> {information.phone}</p>
                    </div>

                    {language === "spanish" ?
                        <button className="developers" onClick={openDevelopers} title="Desarolladores" ><FontAwesomeIcon icon={faLaptopCode} /> Desarolladores</button>
                        :
                        <button className="developers" onClick={openDevelopers} title="Developers" ><FontAwesomeIcon icon={faLaptopCode} /> Developers</button>
                    }

                    <div className="socialsNetworks">
                        <a href={"http://" + information.instagram} target="_blank" ><img src={Instagram} title='Instagram' /></a>
                        <a href={"http://" + information.facebook} target="_blank" ><img src={Facebook} title='Facebook' /></a>
                    </div>
                </footer>
                :
                null
            }
            {window.screen.width > 660 ?
                <Developers devRef={devRef} openDevelopers={openDevelopers} />
                :
                <Carrusel devRef={devRef} openDevelopers={openDevelopers} />
            }
        </div>
    )
}