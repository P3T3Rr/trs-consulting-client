import Linkiedin from "../../../assets/socialNetworks/Linkedin.ico";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Jeremmy from "../../../assets/developers/Jeremmy.jpg";
import Yosiney from "../../../assets/developers/Yosiney.png";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import Emilio from "../../../assets/developers/Emilio.jpg";
import Git from "../../../assets/socialNetworks/git.png";
import Kevin from "../../../assets/developers/Kevin.jpg";
import Brian from "../../../assets/developers/Brian.jpg";
import Josue from "../../../assets/developers/Josue.jpg";
import Diego from "../../../assets/developers/Diego.png";
import Right from "../../../assets/Right.png";
import Left from "../../../assets/Left.png";
import { useSelector } from "react-redux";
import './styles.css';

const devs1 = [
    {
        "name": "Brian Perez",
        "area": "Front-end developer",
        "linkedin": "https://www.linkedin.com/in/brian-perez-52a152191",
        "git": "https://gitlab.com/brianAvila",
        "photo": Brian
    },
    {
        "name": "Josue Huertas",
        "area": "Front-end developer",
        "linkedin": "https://www.linkedin.com/in/joshuertasmora",
        "git": "https://gitlab.com/Josuehm555",
        "photo": Josue
    },
    {
        "name": "Emilio Salas",
        "area": "Back-end developer",
        "linkedin": "https://www.linkedin.com/in/luis-emilio-salas-vargas",
        "git": "https://gitlab.com/luis_sv",
        "photo": Emilio
    },
    {
        "name": "Kevin Solano",
        "area": "Front-end developer",
        "linkedin": "https://www.linkedin.com/in/kevin-solano-6548a9255",
        "git": "https://gitlab.com/kevinsolano14",
        "photo": Kevin
    }
]

const devs2 = [
    {
        "name": "Yosiney Mora",
        "area": "Scrum master",
        "linkedin": "https://www.linkedin.com/in/yosiney-mora-corrales-a11531236",
        "git": "https://gitlab.com/brianAvila",
        "photo": Yosiney
    },
    {
        "name": "Jeremmy Soto",
        "area": "Front-end developer",
        "linkedin": "https://www.linkedin.com/in/jeremmy-soto-60385b129",
        "git": "https://gitlab.com/P3T3Rr",
        "photo": Jeremmy
    },
    {
        "name": "Diego Rojas",
        "area": "Professor",
        "linkedin": "https://www.linkedin.com/in/diego-rojas-vega",
        "git": "https://gitlab.com/brianAvila",
        "photo": Diego
    }
]

export const Carrusel = ({ devRef, openDevelopers }) => {
    const language = useSelector(state => state.language);


    const onClickLeftRight = () => {
        const carrusel1 = document.getElementById("carrusel1");
        carrusel1.style.transform = 'translateX(-100%)';

        const carrusel2 = document.getElementById("carrusel2");
        carrusel2.style.transform = 'translateX(0%)';

        const arrowRight = document.getElementById("right-arrow");
        arrowRight.style.opacity = "0.2";

        const arrowLeft = document.getElementById("left-arrow");
        arrowLeft.style.opacity = "1";
    }

    const onClickLeftLeft = () => {
        const carrusel1 = document.getElementById("carrusel1");
        carrusel1.style.transform = 'translateX(0%)';

        const carrusel2 = document.getElementById("carrusel2");
        carrusel2.style.transform = 'translateX(100%)';

        const arrowRight = document.getElementById("right-arrow");
        arrowRight.style.opacity = "1";

        const arrowLeft = document.getElementById("left-arrow");
        arrowLeft.style.opacity = "0.2";
    }

    return (
        <div ref={devRef} className="developersContainer">
            {language === "spanish" ?
                <button className="closeDevelopers" onClick={openDevelopers} title="Cerrar"><FontAwesomeIcon icon={faTimes} /></button>
                :
                <button className="closeDevelopers" onClick={openDevelopers} title="Close"><FontAwesomeIcon icon={faTimes} /></button>
            }
            <a href="#" onClick={onClickLeftLeft} className="left-arrow" id="left-arrow"><img src={Left} /></a>
            <a href="#" onClick={onClickLeftRight} className="right-arrow" id="right-arrow"><img src={Right} /></a>

            <div id="carrusel1">
                <div className="cards" id="cards1">
                    {devs1.map((element, key) =>
                        <div className="card" key={key}>
                            <div className="content">
                                <div className="front">
                                    <img src={element.photo} alt={element.name} />
                                </div>
                                <div className="back">
                                    <h4>{element.name}</h4>
                                    <p>{element.area}</p>
                                    <p className="moreInfo">Más Información</p>
                                    <div className="webs">
                                        <div id="linkedin">
                                            <a target="_blank" href={element.linkedin} title="Linkedin">
                                                <img src={Linkiedin} alt="Linkedin" />
                                            </a>
                                        </div>
                                        <div id="git">
                                            <a target="_blank" href={element.git} title="Git">
                                                <img src={Git} alt="Git" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4>{element.name}</h4>
                        </div>
                    )}
                </div>
            </div>

            <div id="carrusel2">
                <div className="cards" id="cards2">
                    {devs2.map((element, key) =>
                        <div className="card" key={key}>
                            <div className="content">
                                <div className="front">
                                    <img src={element.photo} alt={element.name} />
                                </div>
                                <div className="back">
                                    <h4>{element.name}</h4>
                                    <p>{element.area}</p>
                                    <p className="moreInfo">Más Información</p>
                                    <div className="webs">
                                        <div id="linkedin">
                                            <a target="_blank" href={element.linkedin} title="Linkedin">
                                                <img src={Linkiedin} alt="Linkedin" />
                                            </a>
                                        </div>
                                        <div id="git">
                                            <a target="_blank" href={element.git} title="Git">
                                                <img src={Git} alt="Git" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4>{element.name}</h4>
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}