import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { addSubcategoryToCart } from "../../store/app/bodyCartSlice";
import { addTotalCart } from "../../store/app/cartTotalSlice";
import "./styles.css";

export const CardSubcategorie = ({ Text, Description, setOpen }) => {
  const subcategories = useSelector((state) => state.bodyCart);

  const dispatch = useDispatch();

  const addCart = () => {
    if (!subcategories.includes(Text)) {
      dispatch(addSubcategoryToCart(Text));
      dispatch(addTotalCart());
    } else {
      setOpen(true);
    }
  };

  return (
    <>
      <div className="cardSubCategorie">
        <div className="subCategorieTittle">
          <h3>{Text} </h3>
        </div>
        <div className="subCategorieDescription">
          {" "}
          <span>{Description} </span>
        </div>
        <div className="go-corner" onClick={addCart}>
          <div className="go-plus">
            <FontAwesomeIcon
              className="cartIconSubCategorie"
              icon={faShoppingCart}
            />
          </div>
        </div>
      </div>
    </>
  );
};
