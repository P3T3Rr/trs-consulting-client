import React from 'react'
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';
import { useDispatch, useSelector } from 'react-redux';
import { Spinner } from '../spinner/spinner';
import { setStateMarker } from '../../store/app/mapMarkerSlice'

const containerStyle = {
    width: '100%',
    height: '20vh',
    border: "1px solid black",
    borderRadius: "10px ",
    marginTop: "15px"
};

const Map =() => {
    
    const options = {
        minZoom: 3,
        maxZoom: 17,
      }

    const center = useSelector(state => state.map.center);
    const marker = useSelector(state => state.mapMarker.marker);
    const dispatch = useDispatch();

    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyDohJZZNx5WWhOYluq3Rxfk8gGPEvmBuVs"
    })

    const [setMap] = React.useState(null)

    const onLoad = React.useCallback(function callback(map) {
        const bounds = new window.google.maps.LatLngBounds(center);
        map.fitBounds(bounds);
        setMap(map)
    }, [])


    const onUnmount = React.useCallback(function callback(map) {
        setMap(null)
    }, [])

    const newMarker = (e) => {
        const newMarker = { "marker": { lat: e.latLng.lat(), lng: e.latLng.lng() } }
        dispatch(setStateMarker(newMarker))
        // {"marker": {lat:parseFloat(lat) ,lng:parseFloat(lon)}}
    }

    return isLoaded ? (

        <GoogleMap
            options={options}
            mapContainerStyle={containerStyle}
            center={center}
            onLoad={onLoad}
            onUnmount={onUnmount}
            onClick={(e) => newMarker(e)}

        >
            <Marker position={marker} />
        </GoogleMap>

    ) : (

        <div className='spinnerMap'> <Spinner /> </div>
    )
}

export default React.memo(Map)
