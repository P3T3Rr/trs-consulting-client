import { faBars, faShoppingCart, faLanguage } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { setLanguage } from "../../store/app/language-slice";
import { openCloseCart } from "../../store/app/cartSlice";
import { useDispatch, useSelector } from "react-redux";
import './styles.css';

export const Buttons = ({ labels, showNavBar }) => {

    const totalCart = useSelector((state) => state.totalCart);
    const dispatch = useDispatch();
    const flag = require('../../assets/flags/' + labels.flag);

    const handleCart = () => {
        dispatch(openCloseCart())
    }

    const changeLanguage = () => {
        dispatch(setLanguage(labels.language))
    }

    return (
        <div>
            <button className="language" title={labels.languageButton} onClick={changeLanguage}>
                <div className="flags" >
                    <img className="flag" src={flag} alt="flag" />
                </div>
                <FontAwesomeIcon icon={faLanguage} />
            </button>

            <button className='car' title={labels.ShoppingCartButton} onClick={handleCart} >
                <FontAwesomeIcon icon={faShoppingCart} />
               {totalCart !== 0 && <span className='numberCar'>{totalCart}</span>}
            </button>

            <button className='nav-btn' onClick={showNavBar} title="Menu">
                <FontAwesomeIcon icon={faBars} />
            </button>
        </div>
    )
}