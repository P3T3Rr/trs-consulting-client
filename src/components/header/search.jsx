import { setServicesFilter } from "../../store/app/servicesFilter-slice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import './styles.css';

export const Search = () => {
    const dispatch = useDispatch();
    const language = useSelector(state => state.language);
    const allServices = useSelector(state => state.services);

    const search = () => {
        console.log("hola");
        let wordFilter = document.getElementById("input-search").value
        let newListServices = [];

        if (wordFilter === '') {
            dispatch(setServicesFilter(allServices));
        }
        else {
            for (let index = 0; index < allServices.length; index++) {
                const name = allServices[index].name.toLowerCase()
                let i = name.indexOf(wordFilter.toLowerCase())

                if (i >= 0) {
                    newListServices.push(allServices[index]);
                }
            }
            dispatch(setServicesFilter(newListServices));
        }

    }

    return (
        <div className="search-container">
            <FontAwesomeIcon className="icon" icon={faSearch} />
            {language === "spanish" ?
                <input onChange={search} id="input-search" type="text" placeholder="Buscar servicio" />
                :
                <input onChange={search} id="input-search" type="text" placeholder="Service search" />
            }
            <div className='span-search'></div>
        </div>
    )
}