import { ShoppingCart } from "../shoppingCart/ShoppingCart";
import labelSpanish from "../../apis/navbar-espanish.json";
import labelEnglish from "../../apis/navbar-english.json";
import Logo from "../../assets/Logo navbar.png";
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Buttons } from "./buttons";
import { Search } from "./search";
import { Navbar } from "./navbar";
import { useRef } from 'react';
import Suscribe from "../suscribe/suscribe"
import './styles.css';

export const Header = () => {
    const navRef = useRef();
    const language = useSelector(state => state.language);

    const showNavBar = () => {
        navRef.current.classList.toggle('responsive_nav');
    }

    return (
        <>
            <div className="headerContainer">
                <header>
                    <Link className='link' to="/home">
                        <img className="logo" src={Logo} alt="Logo" />
                    </Link>

                    <Search />

                    {language === "spanish" ?
                        <Navbar labels={labelSpanish.data} navRef={navRef} showNavBar={showNavBar} />
                        :
                        <Navbar labels={labelEnglish.data} navRef={navRef} showNavBar={showNavBar} />
                    }

                {language === "spanish" ?
                    <Buttons labels={labelSpanish.data} showNavBar={showNavBar} />
                    :
                    <Buttons labels={labelEnglish.data} showNavBar={showNavBar} />
                }
            </header>
        </div>
        <ShoppingCart/>
        <Suscribe/>
       </>

    );
}