import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { NavLink } from 'react-router-dom';
import './styles.css';
import { useDispatch } from "react-redux";
import { openCloseSuscribe } from "../../store/app/suscribeslice";

export const Navbar = ({ labels, navRef, showNavBar }) => {
    const dispatch = useDispatch();

    const openSuscribe = () =>{
        dispatch(openCloseSuscribe());
        showNavBar();
    }
    return (
        <div>
        <nav ref={navRef}>
            <NavLink onClick={showNavBar} className='link' to={"/home"} title={labels.service}>{labels.service}</NavLink>
            <NavLink onClick={showNavBar} className='link' to={"/galery"} title={labels.galery}>{labels.galery}</NavLink>
            <NavLink onClick={showNavBar} className='link' to={"/about-us"} title={labels.aboutus}>{labels.aboutus}</NavLink>

            <button className='subscribe' title={labels.subscribeButton} onClick={() => openSuscribe()}>
                {labels.subscribeButton}
            </button>

            <button className='nav-btn nav-close-btn' onClick={showNavBar}>
                <FontAwesomeIcon icon={faTimes} />
            </button>
        </nav>
        </div>
    )
}
