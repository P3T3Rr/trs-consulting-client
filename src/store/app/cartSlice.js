import { createSlice } from "@reduxjs/toolkit";

export const cartSlice = createSlice({
  name: "cart",
  initialState: true,
  reducers: {
    //puede ser llamado desde cualquier parte de mi app
    //the actions are the params that i will use
    //aqui se asigna ese action al state
    openCloseCart: (state) => {
      return !state;
    },
  },
});

export const { openCloseCart } = cartSlice.actions;
export default cartSlice.reducer;
