import { createSlice } from '@reduxjs/toolkit';
// , { "marker":{lat: -3.745, lng: -38.523}
export const mapSlice = createSlice({
    name: 'map',
    initialState: {"center": {lat: 10.4539194,lng: -84.2747255}
    },
    reducers: {
        setStateCenter: (state, data) => {
            return (data.payload);
        }
    },
});

export const { setStateCenter } = mapSlice.actions;
export default mapSlice.reducer;