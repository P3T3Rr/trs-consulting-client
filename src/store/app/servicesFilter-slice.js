import { createSlice } from '@reduxjs/toolkit';

export const servicesSliceFilter = createSlice({
    name: 'filterServices',
    initialState: [],
    reducers: {
        setServicesFilter: (state, data) => {
            return (data.payload);
        }
    }
});

export const { setServicesFilter } = servicesSliceFilter.actions;
export default servicesSliceFilter.reducer;