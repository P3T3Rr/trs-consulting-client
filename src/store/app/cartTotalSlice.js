import { createSlice } from "@reduxjs/toolkit";

export const cartTotalSlice = createSlice({
  name: "cart",
  initialState: 0,
  reducers: {
    addTotalCart: (state,action) => {
      
      state++
      return state;
    },
    subtractTotalCart: (state,action) => {
      state--
      return state;
    },
  },
});

export const { addTotalCart, subtractTotalCart } = cartTotalSlice.actions;
export default cartTotalSlice.reducer;
