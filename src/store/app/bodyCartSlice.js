import { createSlice } from "@reduxjs/toolkit";

export const bodyCartSlice = createSlice({
  //puedo acceder a estos datos desde cualquier lugar
  name: "bodyCart",
  initialState :  [],
  reducers: {
    //puede ser llamado desde cualquier parte de mi app
    //the actions are the params that i will use
    //aqui se asigna ese action al state
    addSubcategoryToCart: (state, action) => {
        return [...state, action.payload]
    },
    deleteSubcategoryToCart: (state, action) => {
      const subCatorie = action.payload
      return state.filter(element=> !subCatorie.includes(element) )
      
  },
    editSubcategoryCart: (state, action) => {
      return [...state, action.payload]
  }
  },
}
);
//the actions that i will call
export const { addSubcategoryToCart,deleteSubcategoryToCart } = bodyCartSlice.actions;
export default bodyCartSlice.reducer;
