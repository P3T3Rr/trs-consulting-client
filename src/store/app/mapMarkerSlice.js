import { createSlice } from '@reduxjs/toolkit';
// , { "marker":{lat: -3.745, lng: -38.523}
export const mapMarkerSlice = createSlice({
    name: 'marker',
    initialState: {"marker":{lat: 10.4539194,lng: -84.2747255}
    },
    reducers: {
        setStateMarker: (state, data) => {
            console.log("data: " , data , "estado: ", state);
            console.log('devuelvo: ', {...state, data});
            
            return (data.payload);
        }
    },
});

export const { setStateMarker } = mapMarkerSlice.actions;
export default mapMarkerSlice.reducer;