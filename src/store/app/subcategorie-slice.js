import { db } from "../services/firebase-config";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { collection, onSnapshot, query } from "firebase/firestore";
import { createSubcategorieAdapter } from "../adapters/subcategorie-adapter";

export const getSubcategories = createAsyncThunk(
  "subcategories/getSubcategories",
  async (dispatch, getState) => {
    const snap = query(collection(db, "subcategories"));
    onSnapshot(snap, (querySnapshot) => {
      const data = [];
      querySnapshot.forEach((doc) => {
        console.log("Prueba   ", doc.data());
        if (doc.data() !== null) {
          data.push(createSubcategorieAdapter(doc.id, doc.data()));
        }
      });
      return data;
    });
  }
);

const subcategorieSlice = createSlice({
    name: "subcategorie",
    initialState: {
        subcategories: [],
        status: null
    }
})

export default subcategorieSlice.reducer;