import { createSlice } from "@reduxjs/toolkit";

export const suscribeslice = createSlice({
  //puedo acceder a estos datos desde cualquier lugar
  name: "suscribe",
  initialState :  false,
  reducers: {
    //puede ser llamado desde cualquier parte de mi app
    //the actions are the params that i will use
    //aqui se asigna ese action al state
    openCloseSuscribe: (state) => {
        return !state
    },

  },
}
);
//the actions that i will call
export const { openCloseSuscribe } = suscribeslice.actions;
export default suscribeslice.reducer;
