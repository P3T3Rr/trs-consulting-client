import { createSlice } from '@reduxjs/toolkit';

export const servicesSlice = createSlice({
    name: 'allServices',
    initialState: [],
    reducers: {
        setServices: (state, data) => {
            return (data.payload);
        }
    }
});

export const { setServices } = servicesSlice.actions;
export default servicesSlice.reducer;