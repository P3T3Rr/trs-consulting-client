import { createSlice } from "@reduxjs/toolkit";

export const quoteSlice = createSlice({
  name: "quote",
  initialState: false,
  reducers: {
    openCloseQuote: (state) => {
      return !state
    },

  },
}
);

export const { openCloseQuote } = quoteSlice.actions;
export default quoteSlice.reducer;
