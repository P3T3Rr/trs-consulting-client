import { configureStore } from '@reduxjs/toolkit';
import languageSlice from './app/language-slice';
import cartSlice from './app/cartSlice';
import suscribeslice from './app/suscribeslice';
import quoteSlice from './app/quotesSlice';
import mapSlice from './app/mapSlice';
import mapMarkerSlice from './app/mapMarkerSlice';
import bodyCartSlice from './app/bodyCartSlice';
import cartTotalSlice from './app/cartTotalSlice'
import servicesSlice from './app/services-slice';
import servicesSliceFilter from './app/servicesFilter-slice';

export const store = configureStore({
  reducer: {
    services: servicesSlice,
    filterServices: servicesSliceFilter,
    cart: cartSlice,
    language: languageSlice,
    suscribe: suscribeslice,
    quote: quoteSlice,
    map: mapSlice,
    mapMarker: mapMarkerSlice,
    bodyCart: bodyCartSlice,
    totalCart: cartTotalSlice,
  },
})