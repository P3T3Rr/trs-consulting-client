import { Header } from "./components/header/header";
import { Footer } from "./components/footer/footer";
import Routing from "./routes/route";

function App() {

  return (
    <div className="App">
      <Header />
      <Routing />
      <Footer />
    </div>
  );
}

export default App;
