import { Route, Routes, Navigate } from 'react-router-dom';
import { AboutUs } from '../components/about-us/about-us';
import { NotFoundPage } from '../pages/not-found-page';
import { Galery } from '../components/gallery/gallery';
import { Home } from '../pages/home';
import { Subcategories } from '../pages/subcategories';

export default function Routing() {
    return (
        <Routes>
            <Route path='*' element={<NotFoundPage />} />
            <Route path='/' element={<Navigate to="/home" />} />
            <Route path='/home' element={<Home />} />
            <Route path='/galery' element={<Galery />} />
            <Route path='/about-us' element={<AboutUs />} />
            <Route path='/home/:id' element={<Subcategories />} />
        </Routes>
    );
}